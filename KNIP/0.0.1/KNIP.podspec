#
# Be sure to run `pod lib lint KNIP.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'KNIP'
  s.version          = '0.0.1'
  s.summary          = 'The KNIP plugin contains all the Core, Utility, Datamodel, API and UI components needed for KNIP applications.'
  s.description      = <<-DESC
    The KNIP pod contains all the necessary files to get started developing apps for KNIP. It contains the following subscpecs:
        - Core: the core files and utils that are needed for KNIP to function
        - UI: contains Xib's, UIView subclasses etc
        - DataModel: contains data model classes that are responsible for parsing JSON
        - API: supplies APIs that can be used in KNIP applications
    DESC

  s.homepage         = 'https://bitbucket.org/SkopeiDigital/ios-knip'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Jordy van Kuijk' => 'jordy@skopei.com' }
  s.source           = { :git => 'git@bitbucket.org:SkopeiDigital/ios-knip.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'KNIP/Classes/**/*'
  
  # s.resource_bundles = {
  #   'KNIP' => ['KNIP/Assets/*.png']
  # }

  # KNIP Subspecs

  s.subspec 'Core' do |cs|
    cs.source_files = 'KNIP/Classes/Core/**/*'
  end

  s.subspec 'API' do |cs|
    cs.dependency 'KNIP/Core'
    cs.source_files = 'KNIP/Classes/API/**/*'
  end

  s.subspec 'DataModel' do |cs|
    cs.dependency 'KNIP/Core'
    cs.source_files = 'KNIP/Classes/DataModel/**/*'
  end

  s.subspec 'UI' do |cs|
    cs.dependency 'KNIP/Core'
    cs.source_files = 'KNIP/Classes/UI/**/*'
  end

  s.subspec 'Utils' do |cs|
    cs.dependency 'KNIP/Core'
    cs.source_files = 'KNIP/Classes/Utils/**/*'
  end

  s.subspec 'Authentication' do |cs|
    cs.dependency 'KNIP/API'
    cs.dependency 'KNIP/DataModel'
    cs.dependency 'KNIP/UI'
    cs.dependency 'KNIP/Utils'
    cs.source_files = 'KNIP/Classes/Authentication/**/*'
    cs.resource_bundles = {
        'AuthenticationStoryboards' => ['KNIP/Assets/Authentication/*.storyboard']
    }
  end
end
